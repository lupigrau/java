import java.util.Scanner;

public class Vocal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		String palabra;
		int contador = 0;
		System.out.println("Escriba una frase");
		palabra=sc.nextLine();
		for (int x=0;x<palabra.length();x++) {
			if((palabra.toLowerCase().charAt(x)=='a') || (palabra.toLowerCase().charAt(x)=='e') || (palabra.toLowerCase().charAt(x)=='i')||(palabra.toLowerCase().charAt(x)=='o')||(palabra.toLowerCase().charAt(x)=='u')) {
				contador++;	
			}
		}
		System.out.println("La palabra " + palabra + " contiene " + contador +  " vocales");
	}

}

	
