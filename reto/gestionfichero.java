package alumno;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class gestionfichero {
  private String nombrefichero;
  public gestionfichero(String nombrefichero) {
    super();
    this.nombrefichero = nombrefichero;
  }
//metodo de escritura
  public void escritura(Alumno a) {
    FileWriter fw;
    BufferedWriter bw = null;
    String linea = "";
    try {
      fw = new FileWriter(nombrefichero, true);
      bw = new BufferedWriter(fw);
      String dni = a.getDni();
      String nombre = a.getNombre();
      String ciclo = a.getCicloFormativo();
      String modulo = a.getModulo();
      int nota = a.getNota();
      linea = dni + "," + nombre + "," + ciclo + "," + modulo + "," + nota;
      bw.write(linea + "\n");
      bw.close();
      fw.close();
    } catch (IOException e) {
      // TODO Auto-generated catch block					
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  public ArrayList < Alumno > leer(ArrayList < Alumno > lee) {
    String campos[] = null;
    String linea = "";
    ArrayList < Alumno > aux = new ArrayList < Alumno > ();
    try {
      FileReader fr = new FileReader(nombrefichero);
      BufferedReader br = new BufferedReader(fr);
      while (linea != null) {
        linea = br.readLine();
        if (linea != null) {
          campos = linea.split(",");
          Alumno alum = new Alumno();
          alum.setDni(campos[0]);
          alum.setNombre(campos[1]);
          alum.setCicloFormativo(campos[2]);
          alum.setModulo(campos[3]);
          alum.setNota(Integer.parseInt(campos[4]));
          aux.add(alum);
        }
      }
      br.close();
      fr.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NullPointerException e) {
      e.printStackTrace();
    }

    return aux;

  }
  public void consultar(ArrayList < Alumno > alum, String modulo, String nameFile) {
    String linea = "";

    try {
      FileReader fr = new FileReader(nameFile);
      BufferedReader br = new BufferedReader(fr);
      while (linea != null) {
        linea = br.readLine();
      }
      br.close();
      fr.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NullPointerException e) {
      e.printStackTrace();
    }
  }
}

