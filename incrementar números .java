package operador;

public class incremental {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int i=4;
		//incrementa el valor de i por uno
		i++;
		System.out.println("valor de i:" + i);
		//decrementa valor anterior
		i--;
		System.out.println("valor de i:" + i);
		//cuando i está antes que ++ primero te muestra el valor que tiene y luego le suma 1
		System.out.println("otro valor" + i++);
		//en este caso te suma primero uno y luego te muetra el valor
		System.out.println("Incremente otra vez: " + ++i);

	}

}
